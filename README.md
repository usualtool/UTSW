最新版本：https://github.com/usualtool/ut-sw

对词库目录dict加入写的权限。

页面调用lib/utsw.php

运行UTSW::SplitWord($content,$title)

得到3个数组，标题关键词组、与标题关键词组相关段落的关键词组、全文关键词组

运行UTSW::AddWord($keyword)

向词库训练添加一个新的词组

示例

require_once('lib/utsw.php');

$title = "UT框架是什么";

$content="UT框架是基于PHP的多端开发框架，类库完善，适合开发各种类型的应用。UT框架内置几乎所有关系数据库或非关系数据库的类库，拥有可自定义的模板引擎、语言本地化解析器及各种函数库。轻便简易的开发模式使开发者更容易理解流程、上手开发。使用UT虽然需要PHP基础知识，但更多的是对UT函数方法的调用，这将节省更多的开发时间。";

print_r(UTSW::SplitWord($content,$title));

UTSW::AddWord("UT框架");